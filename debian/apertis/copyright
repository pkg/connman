Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/

Files: aclocal.m4
Copyright: 1996-2020, Free Software Foundation, Inc.
License: (FSFULLR and/or GPL-2+) with Autoconf-data exception

Files: client/*
Copyright: 2012-2014, Intel Corporation.
License: GPL-2+

Files: client/tethering.c
 client/tethering.h
Copyright: 2018, GlobalLogic.
 2014, Intel Corporation.
License: GPL-2+

Files: compile
 depcomp
 missing
 test-driver
Copyright: 1996-2020, Free Software Foundation, Inc.
License: GPL-2+ with Autoconf-data exception

Files: configure
Copyright: 1992-1996, 1998-2012, Free Software Foundation, Inc.
License: FSFUL

Files: debian/*
Copyright: 2009-2016 Alexander Sack <asac@debian.org>
            2016-2018 Alf Gaida <agaida@siduction.org>
            2016      Mateusz Łukasik <mati75@linuxmint.pl>
            2010-2013 Mathieu Trudel <mathieu-tl@ubuntu.com>
License: GPL-2+

Files: gdbus/*
Copyright: 2004-2011, Marcel Holtmann <marcel@holtmann.org>
License: GPL-2+

Files: gdhcp/*
Copyright: 2007-2017, Intel Corporation.
License: GPL-2

Files: gsupplicant/*
Copyright: 2007-2017, Intel Corporation.
License: GPL-2

Files: gweb/*
Copyright: 2007-2017, Intel Corporation.
License: GPL-2

Files: include/*
Copyright: 2007-2017, Intel Corporation.
License: GPL-2

Files: include/acd.h
Copyright: 2018, Commend International.
License: GPL-2

Files: include/backtrace.h
Copyright: 2016, Yann E. MORIN <yann.morin.1998@free.fr>.
License: GPL-2

Files: include/inotify.h
 include/session.h
Copyright: 2010-2016, BMW Car IT GmbH.
License: GPL-2

Files: include/tethering.h
Copyright: 2018, GlobalLogic.
 2007-2013, Intel Corporation.
License: GPL-2

Files: install-sh
Copyright: 1994, X Consortium
License: X11

Files: ltmain.sh
Copyright: 1996-2015, Free Software Foundation, Inc.
License: (GPL-2+ and/or GPL-3+) with Libtool exception

Files: m4/*
Copyright: 2004, 2005, 2007-2009, 2011-2015, Free Software
License: FSFULLR

Files: m4/configmake.m4
 m4/ltversion.m4
Copyright: 2004, 2010-2015, Free Software Foundation, Inc.
License: FSFULLR

Files: m4/libtool.m4
Copyright: 1996-2001, 2003-2015, Free Software Foundation, Inc.
License: (FSFULLR and/or GPL-2) with Libtool exception

Files: plugins/*
Copyright: 2007-2017, Intel Corporation.
License: GPL-2

Files: plugins/dundee.c
 plugins/iwd.c
 plugins/session_policy_local.c
Copyright: 2010-2016, BMW Car IT GmbH.
License: GPL-2

Files: plugins/ofono.c
Copyright: 2011-2014, BMW Car IT GmbH.
 2010, Nokia Corporation and/or its subsidiary(-ies).
 2007-2013, Intel Corporation.
License: GPL-2

Files: plugins/vpn.c
Copyright: 2018-2021, Jolla Ltd.
 2007-2013, Intel Corporation.
License: GPL-2

Files: scripts/*
Copyright: 2007-2017, Intel Corporation.
License: GPL-2

Files: scripts/openvpn-script.c
Copyright: 2010-2014, BMW Car IT GmbH.
 2007-2014, Intel Corporation.
License: GPL-2

Files: src/*
Copyright: 2007-2017, Intel Corporation.
License: GPL-2

Files: src/6to4.c
Copyright: Alexey Kuznetsov et al. from iproute2 package.
 2011, Nokia Corporation.
License: GPL-2

Files: src/acd.c
Copyright: 2018, Commend International GmbH.
License: GPL-2

Files: src/backtrace.c
Copyright: 2016, Yann E. MORIN <yann.morin.1998@free.fr>.
 2007-2013, Intel Corporation.
License: GPL-2

Files: src/bridge.c
 src/connection.c
 src/inotify.c
 src/ippool.c
 src/nat.c
 src/session.c
Copyright: 2010-2014, BMW Car IT GmbH.
 2007-2014, Intel Corporation.
License: GPL-2

Files: src/firewall-iptables.c
 src/firewall-nftables.c
 src/stats.c
Copyright: 2010-2016, BMW Car IT GmbH.
License: GPL-2

Files: src/inet.c
Copyright: 2007-2013, Intel Corporation.
 2003-2006, Helsinki University of Technology
 2003-2005, Go-Core Project
License: GPL-2

Files: src/nostats.c
Copyright: 2018, Chris Novakovic
License: GPL-2

Files: src/shared/*
Copyright: 2018, Commend International.
 2009, 2010, Aldebaran Robotics.
License: GPL-2

Files: src/shared/mnlg.c
 src/shared/mnlg.h
Copyright: no-info-found
License: GPL-2+

Files: src/shared/util.c
 src/shared/util.h
Copyright: 2012, Intel Corporation.
License: LGPL-2.1+

Files: src/tethering.c
Copyright: 2011, ProFUSION embedded systems
 2007-2013, Intel Corporation.
License: GPL-2

Files: tools/*
Copyright: 2007-2017, Intel Corporation.
License: GPL-2

Files: tools/ip6tables-test.c
Copyright: 2018, Jolla Ltd.
 2013, BMW Car IT GmbH.
 2007-2012, Intel Corporation.
License: GPL-2

Files: tools/iptables-test.c
Copyright: 2010-2014, BMW Car IT GmbH.
 2007-2014, Intel Corporation.
License: GPL-2

Files: tools/iptables-unit.c
 tools/manager-api.c
 tools/session-api.c
 tools/session-test.c
 tools/session-utils.c
 tools/stats-tool.c
Copyright: 2010-2016, BMW Car IT GmbH.
License: GPL-2

Files: tools/private-network-test.c
Copyright: 2012, Intel Corporation.
 2011, ProFUSION embedded systems
License: GPL-2

Files: unit/*
Copyright: 2019, Jolla Ltd.
License: GPL-2

Files: unit/test-ippool.c
Copyright: 2010-2016, BMW Car IT GmbH.
License: GPL-2

Files: vpn/*
Copyright: 2007-2017, Intel Corporation.
License: GPL-2

Files: vpn/plugins/*
Copyright: 2019-2021, Jolla Ltd.
 2012, 2013, Intel Corporation.
 2010, 2013, 2014, BMW Car IT GmbH.
License: GPL-2

Files: vpn/plugins/libwireguard.c
Copyright: 2015-2019, Jason A. Donenfeld <Jason@zx2c4.com>.
 2008-2012, Pablo Neira Ayuso <pablo@netfilter.org>.
License: LGPL-2.1

Files: vpn/plugins/openconnect.c
Copyright: 2018-2021, Jolla Ltd.
 2007-2013, Intel Corporation.
License: GPL-2

Files: vpn/plugins/openvpn.c
Copyright: 2016-2019, Jolla Ltd.
 2010-2014, BMW Car IT GmbH.
License: GPL-2

Files: vpn/plugins/vpn.c
Copyright: 2007-2017, Intel Corporation.
License: GPL-2

Files: vpn/plugins/vpn.h
Copyright: 2010-2016, BMW Car IT GmbH.
License: GPL-2

Files: vpn/plugins/vpnc.c
Copyright: 2010-2014, BMW Car IT GmbH.
 2007-2014, Intel Corporation.
License: GPL-2

Files: vpn/plugins/wireguard.c
Copyright: 2019, Daniel Wagner.
License: GPL-2

Files: vpn/plugins/wireguard.h
Copyright: 2015-2019, Jason A. Donenfeld <Jason@zx2c4.com>.
License: LGPL-2.1

Files: vpn/vpn-agent.c
 vpn/vpn-provider.c
 vpn/vpn-provider.h
 vpn/vpn-settings.c
Copyright: 2018-2021, Jolla Ltd.
 2007-2013, Intel Corporation.
License: GPL-2

Files: vpn/vpn-util.c
Copyright: 2020, Open Mobile Platform LLC.
 2020, Jolla Ltd.
License: GPL-2

Files: * INSTALL Makefile.in doc/*
Copyright: 2007-2018 Intel Corporation. All rights reserved
License: GPL-2
