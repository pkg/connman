From: Philip Withnall <philip.withnall@collabora.co.uk>
Date: Fri, 13 Mar 2015 01:31:58 -0700
Subject: =?utf-8?q?device=3A_Don=E2=80=99t_report_EALREADY_if_enabling_dev?=
 =?utf-8?q?ice_succeeds_with_ifup?=
MIME-Version: 1.0
Content-Type: text/plain; charset="utf-8"
Content-Transfer-Encoding: 8bit

If a device is indexed (for example, a wired ethernet connection), it
can be successfully enabled by calling connman_inet_ifup(). This means
that the call to device->driver->enable() lower down in
__connman_device_enable() will fail with EALREADY. Squash that error,
since the overall operation has been a success.

This can be reproduced on a system with a wired connection using
connmanctl:
    $ connmanctl technologies
    /net/connman/technology/ethernet
      Name = Wired
      Type = ethernet
      Powered = True
      Connected = True
      Tethering = False
    $ connmanctl disable ethernet
    Disabled ethernet
    $ connmanctl technologies
    /net/connman/technology/ethernet
      Powered = False
      …
    $ connmanctl enable ethernet
    Error ethernet: Already enabled

With the patch applied, the final call succeeds as expected.

Forwarded: https://lists.01.org/pipermail/connman/2015-March/019573.html
Forwarded: https://www.mail-archive.com/connman@connman.net/msg17445.html
Forwarded: https://lists.connman.net/pipermail/connman/2015-March/018876.html
Applied-upstream: no, superseded by https://lists.01.org/pipermail/connman/2015-March/019624.html (?)
---
 src/device.c | 15 ++++++++++-----
 1 file changed, 10 insertions(+), 5 deletions(-)

--- a/src/device.c
+++ b/src/device.c
@@ -189,7 +189,7 @@
 
 int __connman_device_enable(struct connman_device *device)
 {
-	int err;
+	int err, index_err = -EOPNOTSUPP;
 
 	DBG("device %p", device);
 
@@ -207,9 +207,9 @@
 		return -EALREADY;
 
 	if (device->index > 0) {
-		err = connman_inet_ifup(device->index);
-		if (err < 0 && err != -EALREADY)
-			return err;
+		index_err = connman_inet_ifup(device->index);
+		if (index_err < 0 && index_err != -EALREADY)
+			return index_err;
 	}
 
 	device->powered_pending = PENDING_ENABLE;
@@ -218,9 +218,14 @@
 	/*
 	 * device gets enabled right away.
 	 * Invoke the callback
+	 *
+	 * If device->driver->enable() returned EALREADY but the earlier
+	 * connman_inet_ifup() call did not, then the interface came up
+	 * with the earlier call and we should not report an error.
 	 */
-	if (err == 0) {
+	if (err == 0 || (err == -EALREADY && index_err == 0)) {
 		connman_device_set_powered(device, true);
+		err = 0;
 		goto done;
 	}
 
